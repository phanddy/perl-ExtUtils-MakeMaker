%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\(File::Spec\\)\s*$
%global __provides_exclude %{?__provides_exclude:%__provides_exclude|}^perl\\(DynaLoader|ExtUtils::MakeMaker::_version\\)

Name:    perl-ExtUtils-MakeMaker
Epoch:   1
Version: 7.46
Release: 1
Summary: Create Makefile
License: Artistic or GPL+
URL:     https://metacpan.org/release/ExtUtils-MakeMaker
Source0: https://cpan.metacpan.org/authors/id/B/BI/BINGOS/ExtUtils-MakeMaker-%{version}.tar.gz

Patch0:  disable-rpath-by-default.patch
Patch1:  ExtUtils-MakeMaker-7.30-Link-to-libperl-explicitly-on-Linux.patch

Provides:perl-ExtUtils-MM-Utils = %{version}-%{release}
Obsoletes:perl-ExtUtils-MM-Utils < %{version}-%{release}

BuildArch:     noarch
BuildRequires: coreutils, make, sed, perl-generators, perl-interpreter, perl(Carp), perl(Config), perl(Cwd), perl(Encode), perl(Exporter), git
BuildRequires: perl(File::Basename), perl(File::Copy), perl(File::Path), perl(File::Spec) >= 0.8 perl(lib), perl(strict), perl(vars), perl(version), perl(warnings)
BuildRequires: perl-ExtUtils-ParseXS, perl(base), perl(AutoSplit), perl(CPAN::Meta) >= 2.143240
BuildRequires: perl(Data::Dumper), perl(DynaLoader), perl(File::Find), perl(Parse::CPAN::Meta) >= 1.4414
BuildRequires: perl(ExtUtils::Install) >= 1.52, perl(ExtUtils::Manifest) >= 1.70, perl(File::Temp) >= 0.22
BuildRequires: perl(File::Find), perl(Getopt::Long), perl(less), perl(POSIX), perl(subs), perl(overload) perl(Pod::Man)
BuildRequires: perl(B), perl(ExtUtils::CBuilder), perl(PerlIO), perl(utf8), perl-devel, perl(Test::More), perl(Test::Harness)
Requires:      perl(:MODULE_COMPAT_%(eval "`perl -V:version`"; echo $version)), perl(Data::Dumper), perl(DynaLoader), perl(B)
Requires:      perl(Encode), perl(ExtUtils::Command) >= 1.19, perl(ExtUtils::Install) >= 1.54, perl-ExtUtils-ParseXS
Requires:      perl(ExtUtils::Manifest) >= 1.70 perl(File::Find), perl(File::Spec) >= 0.8, perl(Getopt::Long), perl(Pod::Man), perl(POSIX), perl(Test::Harness)
Recommends:    perl(Time::HiRes), perl-devel, perl(Encode::Locale), perl(CPAN::Meta::Requirements) >= 2.130, perl(CPAN::Meta) >= 2.143240
Suggests:      perl(JSON::PP), perl(CPAN::Meta::Converter) >= 2.141170

%description
This utility is designed to write a Makefile for an extension module from a Makefile.PL.
It is based on the Makefile.SH model provided by Andy Dougherty and the perl5-porters.

It splits the task of generating the Makefile into several subroutines that can be individually overridden.
Each subroutine returns the text it wishes to have written to the Makefile.

As there are various Make programs with incompatible syntax, which use operating system shells, again with incompatible syntax,
it is important for users of this module to know which flavour of Make a Makefile has been written for so they'll use the correct one
and won't have to face the possibly bewildering errors resulting from using the wrong one.

On POSIX systems, that program will likely be GNU Make; on Microsoft Windows, it will be either Microsoft NMake, DMake or GNU Make.
See the section on the "MAKE" parameter for details.

ExtUtils::MakeMaker (EUMM) is object oriented. Each directory below the current directory that contains a Makefile.PL is treated as a separate object.
This makes it possible to write an unlimited number of Makefiles with a single invocation of WriteMakefile().

All inputs to WriteMakefile are Unicode characters, not just octets. EUMM seeks to handle all of these correctly.
It is currently still not possible to portably use Unicode characters in module names, because this requires Perl to handle Unicode filenames,
which is not yet the case on Windows.

%package -n perl-ExtUtils-Command
Summary:   Utilities to replace common UNIX commands in Makefiles
License:   Artistic or GPL+
Requires:  perl(File::Path), perl(File::Find), perl(File::Copy), perl(:MODULE_COMPAT_%(eval "`perl -V:version`"; echo $version)) perl(Carp)
BuildArch: noarch

%description -n perl-ExtUtils-Command
The module is used to replace common UNIX commands. In all cases the functions work from @ARGV rather than taking arguments.
This makes them easier to deal with in Makefiles.

%package_help

%prep
%autosetup -n ExtUtils-MakeMaker-%{version} -p1 -Sgit

%build
export BUILDING_AS_PACKAGE=1
%{__perl} Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 NO_PERLLOCAL=1
%make_build

%install
%make_install
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%doc CONTRIBUTING README
%{perl_vendorlib}/*
%{_bindir}/*
%exclude %{perl_vendorlib}/ExtUtils/Command.pm

%files -n perl-ExtUtils-Command
%dir %{perl_vendorlib}/ExtUtils
%{perl_vendorlib}/ExtUtils/Command.pm

%files help
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Wed Jul 23 2020 xinghe <xinghe1@huawei.com> - 1:7.46-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 7.46

* Fri Jan 28 2020 openEuler Buildteam <buildteam@openeuler.org> - 1:7.42-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete unneeded provides and requires

* Thu Jan 2 2020 openEuler Buildteam <buildteam@openeuler.org> - 1:7.42-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 7.42

* Sun Sep 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.7.34-421
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: revise changelog

* Thu Sep 26 2019 wubo<wubo40@huawei.com> - 1.7.34-420
- Type:enhancemnet
- ID:NA
- SUG:restart
- DESC: openEuler Debranding

* Tue Sep 17 2019 wubo<wubo40@huawei.com> - 1.7.34-419
- Package init

